<?php

namespace App\Http\Controllers;
use App\Postagem;
use Dotenv\Validator;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $registros = Postagem::where('ativa', 's')->get();
        return view('public', compact('registros'));
    }

    public function postagem($id)
    {
        $registro = Postagem::find($id);
        return view('public_post', compact('registro'));
    }

}
