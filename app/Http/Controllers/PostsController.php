<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postagem;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function novo()
    {
        return view('novo');
    }

    public function index()
    {
        $registros = Postagem::all();
        return view('home', compact('registros'));
    }

    public function adicionar()
    {
        return view('adicionar');
    }


    // Inserir dados novas
    public function salvar(Request $req)
    {
        $dados = $req->all();

        if(isset($dados['ativa']))
        {
            $dados['ativa'] = 's';
        }else{
            $dados['ativa'] = 'n';
        }

        if($req->hasFile('imagem'))
        {
            $imagem = $req->file('imagem');
            $num = rand(1111,9999);
            $dir = "img/postagem";
            $ex = $imagem->guessClientExtension();
            $nomeImagem = "imagem_".$num.".".$ex;
            $imagem->move($dir,$nomeImagem);
            $dados['imagem'] = $dir."/".$nomeImagem;
        }
        Postagem::create($dados);
        return redirect()->route('home');
    }

    // Editar dados
    public function editar($id)
    {
        $registro = Postagem::find($id);
        return view('editar', compact('registro'));
    }

    public function atualizar(Request $req, $id)
    {

        $dados = $req->all();

        if(isset($dados['ativa']))
        {
            $dados['ativa'] = 's';
        }else{
            $dados['ativa'] = 'n';
        }

        if($req->hasFile('imagem'))
        {
            $imagem = $req->file('imagem');
            $num = rand(1111,9999);
            $dir = "img/postagem/";
            $ex = $imagem->guessClientExtension();
            $nomeImagem = "imagem_".$num.".".$ex;
            $imagem->move($dir,$nomeImagem);
            $dados['imagem'] = $dir."/".$nomeImagem;
        }
        Postagem::find($id)->update($dados);
        return redirect()->route('home');
    }

    public function deletar($id)
    {
        Postagem::find($id)->delete();
        return redirect()->route('home');
    }

}
