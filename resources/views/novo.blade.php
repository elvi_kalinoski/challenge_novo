@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nova postagem</div>
                <form class="" action="{{ route('salvar')}}" method="POST" enctype="multipart/form-data" id="form_cadastra">
                    {{ csrf_field() }}
                    <!-- Formulario -->

                    <!-- Campo Título -->
                    <div class="form-group">
                        <label for="form-titulo">Título</label>
                        <input type="text" class="form-control" name="titulo" value="{{ isset($registro->titulo) ? $registro->titulo : ''}}">
                    </div>

                    <!-- Campo Descrição -->
                    <div class="form-group">
                        <label for="form-descricao">Descrição</label>
                        <input type="text" class="form-control" name="descricao" value="{{ isset($registro->descricao) ? $registro->descricao : ''}}">
                    </div>

                    <!-- Importar Imagem -->
                    <span>Imagem</span>
                    <div class="file-field input-field">
                        <div class="btn blue file-path-wrapper">
                            <input class="file-path validate" type="file" name="imagem">
                        </div>
                    </div>

                    <!-- Mostra a imagem -->
                    @if(isset($registro->imagem))
                    <div class="form-group">
                        <img src="{{ asset($registro->imagem) }}" alt="">
                    </div>
                    @endif

                    <!-- Checkbox para ativar ou não a postagem -->
                    <div class="form-group">
                        <p>
                            <label>
                                <input type="checkbox" name="ativa" {{ isset($registro->ativa) && $registro->ativa == 's' ? 'checked' : '' }} value="true"/>
                                <span>Ativa</span>
                            </label>
                        </p>
                    </div>

                    <div class="container">
                        <div class="progress">
                          <div id="myBar" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only"></span>
                          </div>
                        </div>
                      </div>

                    <br>
                    <div id="cria_post">
                        <button class="btn btn-primary" type="submit">Salvar</button>
                    </div>
                </form>
 
                <div class="card-body">
                    <b>|| Adicione aqui o cadastro da postagem, campos na base de dados, tabela POSTAGEM ||</b>
                    <br>
                    <b>|| Usar AJAX no submit e uma barra de progresso (envio em % ou bytes, qualquer comunicação de andamento) ||</b>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="{{ asset("js/jquery.js") }}"></script>
<script src="{{ asset("js/novo_post.js") }}"></script>

<script>
    
var i = 0;
$(function () {
    $("#form_cadastra").submit(function () 
        var vazios = $("input[type=text]").filter(function() {
            return !this.value;
        }).get();
        var vazios = $("input[type=file]").filter(function() {
            return !this.value;
        }).get();

        if (vazios.length) {
            $(vazios).addClass('vazio');
            alert("Todos os campos devem ser preenchidos.");
            return false;
        } else {
            alert("Cadastro realizado com sucesso!");
        }
    });
});


if (i == 0) {
            i = 1;
            var elem = document.getElementById("myBar");
            var width = 1;
            var id = setInterval(frame, 10);
            function frame() {
            if (width >= 100) {
                clearInterval(id);
                i = 0;
            } else {
                width++;
                elem.style.width = width + "%";
            }
            }
        }

</script>

@endsection



