@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $registro->titulo }}</div>

                <div class="card-body">
                    <img src="{{asset($registro->imagem)}}" class="card-img-top" alt="...">
                    <p class="card-text">{{ $registro->descricao }} </p>
                </div>
                <a class="btn btn-primary" href="/">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
