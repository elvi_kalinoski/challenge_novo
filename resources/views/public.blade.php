@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Postagens</div>

                <div class="card-img-top">
                    <b>|| Adicione aqui as postagens ativas ||</b>

                    @foreach ($registros as $registro)
                    <div class="card" style="width: 15rem;">
                        <img src="{{asset($registro->imagem)}}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">{{ $registro->titulo }}</h5>
                            <p class="card-text">{{ $registro->descricao }}</p>
                            <!--<a href="{{ URL::to('postagem') }}" class="btn btn-primary">Abrir postagem</a>-->
                            <a class="btn btn-primary" href="{{ route('public_post',$registro->id) }}">Abrir postagem</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
