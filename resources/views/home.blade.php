@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Postagens</div>

                <button type="button" class="btn btn-labeled btn-success col-2 m-2" onclick="window.location='{{ URL::to('posts/novo') }}'">
                    + Nova
                </button>
                <div class="container">
                    <div class="row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Descrição</th>
                                    <th>Imagem</th>
                                    <th>Status</th>
                                    <th>Ativa / Desativa</th>
                                    <th>Deletar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($registros as $registro)
                                    <tr>
                                        <td>{{ $registro->titulo }}</td>
                                        <td>{{ $registro->descricao }}</td>
                                        <td><img height="60" src="{{ asset($registro->imagem) }}" alt="{{ $registro->titulo }}"></td>
                                        <td>{{ $registro->ativa }}</td>
                                        <td>
                                            <form class="" action="{{ route('atualizar',$registro->id)}}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="put">
                                                <button class="btn btn-primary" name="ativa" value="true">Publicar</button>
                                            </form>
                                            <br>
                                            <form class="" action="{{ route('atualizar',$registro->id)}}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="put">
                                                <button class="btn btn-primary" name="ativa">Despublicar</button>
                                            </form>
                                        </td>
                                        <td>
                                            <a class="btn red" href="{{ route('deletar',$registro->id) }}">Deletar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="card-body">
                    <b>|| Adicione aqui uma listagem de postagens, com botão de publicar e remover ||</b>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection
