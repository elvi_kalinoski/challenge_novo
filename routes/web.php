<?php
Auth::routes();

Route::get('/posts/novo', 'PostsController@novo');                                      // Adiciona nova postagem
Route::get('/',['as'=>'home','uses'=>'PublicController@index']);                        // Página inicial
Route::get('/public', 'PublicController@public');                                       //Postagens publicas
Route::get('/home',['as'=>'home','uses'=>'PostsController@index']);                     //Home
Route::get('/postagem/{id}',['as'=>'public_post','uses'=>'PublicController@postagem']); //Gerenciamento de postagen


// rotas para gerenciamento do administrador
Route::group(['middleware' => 'auth'], function () {
    Route::get('/adicionar',['as'=>'adicionar','uses'=>'PostsController@adicionar']);       //Adicionar
    Route::post('/salvar',['as'=>'salvar','uses'=>'PostsController@salvar']);               //Salvar
    Route::get('/editar/{id}',['as'=>'editar','uses'=>'PostsController@editar']);           //Editar
    Route::put('/atualizar/{id}',['as'=>'atualizar','uses'=>'PostsController@atualizar']);  //Atualizar
    Route::get('/deletar/{id}',['as'=>'deletar','uses'=>'PostsController@deletar']);        //Deletar
});
